package w.fabian;

import java.time.LocalDateTime;

public class StreamerData {

    private String name;
    private String status;
    private String streamTitle;
    private String streamURL;
    private boolean isLive;
    private LocalDateTime startTime;

    public StreamerData(String name, String streamTitle, String streamURL, boolean isLive, LocalDateTime startTime) {
        this.name = name;
        this.status = isLive?"\uD83D\uDFE2":"🔴";
        this.streamTitle = streamTitle;
        this.streamURL = streamURL;
        this.isLive = isLive;
        this.startTime = startTime;
    }

    public StreamerData(String name, String streamURL, boolean isLive) {
        this.name = name;
        this.status = isLive?"\uD83D\uDFE2":"🔴";
        this.streamURL = streamURL;
        this.isLive = isLive;
    }

    public String getName() {
        return name;
    }

    public String getStreamTitle() {
        return streamTitle;
    }

    public String getStreamURL() {
        return streamURL;
    }

    public boolean isLive() {
        return isLive;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Streamer = ").append(name).append('\n');
        sb.append("Status = ").append(status).append('\n');
        sb.append("Title = ").append(streamTitle).append('\n');
        sb.append("Link = ").append(streamURL).append('\n');
        sb.append("isLive = ").append(isLive).append('\n');
        sb.append("StartTime = ").append(startTime);
        return sb.toString();
    }
}
