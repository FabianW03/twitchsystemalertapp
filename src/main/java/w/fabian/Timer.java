package w.fabian;

import java.time.LocalTime;

public class Timer {

    private static LocalTime start;
    private static LocalTime end;
    private static Long operatedTime;

    public static void startTimer() {
        start = LocalTime.now();
    }

    public static void stopTimer() {
        if (start != null) {
            end = LocalTime.now();
            operatedTime = (long) (end.toSecondOfDay() - start.toSecondOfDay());
        } else {
            System.err.println("Timer has not been started!");
        }
    }

    public static Long getOperatedTime() {
        if (operatedTime != null) {
            return operatedTime;
        } else {
            System.err.println("Timer has not been started!");
            return 0L;
        }
    }

    public static void printTimeOut() {
        if (operatedTime != null) {
            System.out.println("Timer has tracked " + operatedTime + " seconds.");
        } else {
            System.err.println("No timer was started!");
        }

    }

}
