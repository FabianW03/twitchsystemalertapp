package w.fabian;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class LiveDetector {

    public static Document getConnection(String url) {
        try {
            return Jsoup.connect(url)
                    .data("query", "java")
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
                    .referrer("http://www.google.com")
                    .cookie("auth", "token")
                    .timeout(3000)
                    .get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static StreamerData checkForStreamerActivity(String streamerName) {
        String streamURL = "https://www.twitch.tv/" + streamerName.toLowerCase();
        Document html = getConnection(streamURL);

        if (html.head().toString().contains("isLiveBroadcast")) {
            String responseContext = html.head().getElementsByTag("script").get(0).dataNodes().get(0).getWholeData();
            responseContext = responseContext.substring(2,responseContext.length()-2);

            List<String> respList = Arrays.stream(responseContext.split(",")).map(str -> str.replaceAll("\"", " ")).toList();

            String description = "";
            LocalDateTime startDate = LocalDateTime.now();
            for (String str:respList) {
                if (str.contains("description"))
                    description = str.split(" : ")[1].trim();
                if (str.contains("startDate"))
                    startDate = LocalDateTime.parse(str.split(" : ")[1].replace("Z ",""));
            }

            return new StreamerData(
                    streamerName,
                    description,
                    streamURL,
                    respList.stream().anyMatch(str -> str.contains("isLiveBroadcast")),
                    startDate
            );
        } else {
            return new StreamerData(
                    streamerName.replace(streamerName.substring(0,1),streamerName.substring(0,1).toUpperCase()),
                    streamURL,
                    false
            );
        }
    }

}
