package w.fabian;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Timer.startTimer();
        List<String> nameList = Arrays.asList("Zentreya","ironmouse","FerretSoftware","TobiasFate","Tectone","PirateSoftware");

        List<StreamerData> liveStreamers = new ArrayList<>(nameList.stream().map(LiveDetector::checkForStreamerActivity).toList());
        liveStreamers.forEach(System.out::println);
        Timer.stopTimer();

        Timer.printTimeOut();
    }
}